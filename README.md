Exercice Technique - Arca
============

##Le projet
Ce projet a été réalisé dans le cadre d'un exercice technique. Pour plus d'informations concernant son déroulement, vous trouverez une pdf dans le dossier documentation.

##Installation

###Reprise du code
Le projet a été réalisé avec l'IDE Eclipse. Il comporte une arborescence différente des autres projets existant en Java EE.
Veillez donc à utiliser ce même IDE pour reprendre le projet

####Configuration
Ouvrez donc le projet sous Eclipse.
Allez dans le package 
```
src/fr/cestdoncvrai/arca/exercice/Util
```
et ouvrez le fichier 
```
Conf.java
```
Deux fichiers sont renseignés par défaut. remplacez
```
/usr/share/tomcat/webapps/smallData.txt
```
par le chemin vers votre fichier de données.
Puis remplacez 
```
(isOnLine) ? "arca" : "root";
```
Par le nom de votre utilisateur mysql et renseignez le mot de passe de la même façon (DATABASE_PASSWORD).
Remplcez également  
```
(FILE == FILE_BIG) ? "exercice_all" : "exercice" 
```
par le nom de votre base de donnée.

####La base de données
Créer votre base de données si ce n'est pas encore fait et exécuté le script sql se trouvant dans le fichier
```
documentation/myBase.txt
```


###Déploiement du projet
####Prérequis
- Avoir un serveur d'application fonctionnel (Tomcat 7 dans notre exemple)
- Mysql est installé sur le pc
- Avoir générer un .war avec eclipse

####Procédure

#####La base de donnée

#####Déployer
Copiez le fichier arca.war dans le dossier webapss de votre serveur d'application. Tomcat va décompresser l'application. Vous pourrez voir le dossier arca se créer.

#####Testez
L'application est maintenant disponible à l'adresse: 
```
localhost:8000/arca/home
```
Remplacez 8000 par le port de Tomcat