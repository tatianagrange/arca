<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Arca Computing Exercice - Home</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="Project for Arca Computing">
	<meta name="author" content="Tatiana Grange">
	
	<!-- Style -->
	<link href="<c:url value="/bootstrap/css/bootstrap.css"/>"
		rel="stylesheet">
	<link href="<c:url value="style.css"/>"
		rel="stylesheet">

	
	<!-- Scripts -->
	<script src="http://code.jquery.com/jquery-latest.min.js"
        type="text/javascript"></script>
   	<script src="http://www.amcharts.com/lib/amcharts.js" type="text/javascript"></script>
</head>

<body>
	<!-- Part 1: Wrap all page content here -->
	<div id="wrap">
		<c:import url="/inc/menu.jsp" />
	<!-- Begin page content -->
	<section class="container">
		<header>
			<div class="page-header">
				<h1>Home</h1>
			</div>
		</header>	
		<div class="container-fluid">
			<div class="row-fluid">
				<div class="span7">
					<section>
						<header><h2>Table of sums <button id="reloadTableSum" class="btn btn-link" type="button"><i class="icon-refresh"></i></button></h2></header>
						<div id="tableSum">
							<img class="myLoader" src="<c:url value="/img/loader.gif"/>" alt="Loading">
						</div>
					</section>
				</div>
				<div class="span5">
					<div class="well sidebar-nav">
						<h2>Database Sate</h2>
			        	<section id="databaseState">
			        		<section>
				        		<header><h3>Progression</h3></header>
				        		<div id="barContent" class="progress progress-striped active customBar">
									<div class="bar" id="getProgressBar"></div>
								</div>
							</section>
			        		<section>
			        			<header><h3>Actions</h3></header>
			        			<div class="row-fluid">
			        				<div class="span7">
			        					<button id="btn-generate" class="btn btn-large btn-success" type="button">Generate Database</button>
			        				</div>
			        				<div class="span5">
					        			<article>
					        				<button id="btn-truncate" class="btn btn-small btn-danger mySmallButtons" type="button">Clear Database</button>
					        			</article>
					        			<article>
					        				<button id="btn-stop" class="btn btn-small btn-warning mySmallButtons" type="button">Stop Generation</button>
					        			</article>
				        			</div>
			        			</div>
			        		</section>
			        	</section>
		        	</div>
	        	</div>
			</div>
		</div>
		<section>
			<header><h2>Chart</h2></header>
			<div id="chartdiv" style="width: 100%; height: 362px;"></div>
		</section>
		<footer>
			<small>
				<p>The charts are using <a href="http://www.amcharts.com/">http://www.amcharts.com/</a> - © Copyright Tatiana Grange</p>
			</small>
		</footer>
	</section>
	</div>
		
	<!-- <script src="<c:url value="/js/charts.js"/>" type="text/javascript"></script> -->
	<script>
	
	var chart;
	var chartData = [];
	
	
		// this method is called when chart is first inited as we listen for "dataUpdated" event
		function zoomChart() {
		    // different zoom methods can be used - zoomToIndexes, zoomToDates, zoomToCategoryValues
		    chart.zoomToIndexes(0, 10);
		}
		
		$(document).ready(function() {
			//Get the progress for the page
			$.ajax({
        		url: '<c:url value="/database/getProgress"/>',
        		type: "GET",
                success: function(donnees){
                	if(donnees != "Error")
                	{
                		startShowing();
                	}
                	
                	
                }
        	});
			
			//Get the table
			loadTableSum();

	    		
    		$.ajax({
    			url: '<c:url value="/home/generateChart"/>',
    			type: "GET",
    	        success: function(donnees){
    	        	var substr = donnees.split('//');
   				    for (var i = 0; i < substr.length ; i++) {
   				    	var sub = substr[i].split(',');
						var mDate = new Date(sub[0]*1000);
						var visits = sub[1];
   				        chartData.push({
   				            date: mDate,
   				            visits: visits
   				        });
   				    }

   				    // SERIAL CHART    
   				    chart = new AmCharts.AmSerialChart();
   				    chart.marginTop = 0;
   				    chart.autoMarginOffset = 5;
   				    chart.pathToImages = "http://www.amcharts.com/lib/images/";
   				    chart.zoomOutButton = {
   				        backgroundColor: '#000000',
   				        backgroundAlpha: 0.15
   				    };
   				    chart.dataProvider = chartData;
   				    chart.categoryField = "date";

   				    // listen for "dataUpdated" event (fired when chart is inited) and call zoomChart method when it happens
   				    chart.addListener("dataUpdated", zoomChart);

   				    // AXES
   				    // category                
   				    var categoryAxis = chart.categoryAxis;
   				    categoryAxis.parseDates = true; // as our data is date-based, we set parseDates to true
   				    categoryAxis.minPeriod = "DD"; // our data is daily, so we set minPeriod to DD
   				    categoryAxis.dashLength = 2;
   				    categoryAxis.gridAlpha = 0.15;
   				    categoryAxis.axisColor = "#DADADA";

   				    // first value axis (on the left)
   				    var valueAxis1 = new AmCharts.ValueAxis();
   				    valueAxis1.axisColor = "#FF6600";
   				    valueAxis1.axisThickness = 2;
   				    valueAxis1.gridAlpha = 0;
   				    chart.addValueAxis(valueAxis1);

   				    // GRAPHS
   				    // first graph
   				    var graph1 = new AmCharts.AmGraph();
   				    graph1.valueAxis = valueAxis1; // we have to indicate which value axis should be used
   				    graph1.title = "red line";
   				    graph1.valueField = "visits";
   				    graph1.bullet = "round";
   				    graph1.hideBulletsCount = 30;
   				    chart.addGraph(graph1);

   				    // CURSOR
   				    var chartCursor = new AmCharts.ChartCursor();
   				    chartCursor.cursorPosition = "mouse";
   				    chart.addChartCursor(chartCursor);

   				    // SCROLLBAR
   				    var chartScrollbar = new AmCharts.ChartScrollbar();
   				    chart.addChartScrollbar(chartScrollbar);

   				    // LEGEND
   				    var legend = new AmCharts.AmLegend();
   				    legend.marginLeft = 110;
   				    chart.addLegend(legend);

   				    // WRITE
   				    chart.write("chartdiv");
    	        }
    		});
	    	
		});
	
		$('#reloadTableSum').click(function(){
			$('#tableSum').html('<img class="myLoader" src="<c:url value="/img/loader.gif"/>" alt="Loading">');
			loadTableSum();
		});
		
		$('#btn-stop').click(function(){
			stopShowing();
			$.ajax({
        		url: '<c:url value="/database/stop"/>',
        		type: "GET"
        	});
		});
		
        $('#btn-generate').click(function() {
           	startShowing();
        	$.ajax({
        		url: '<c:url value="/database/generate"/>',
        		type: "GET",
                success: function(donnees){
                    stopShowing();
                }
        	});
        });
        
        $('#btn-truncate').click(function() {
        	if($('#btn-truncate').hasClass('disabled'))
        		return false;
        	
			$.ajax({
        		url: '<c:url value="/database/stop"/>',
        		type: "GET",
        		complete: function() 
        		{
                	$.ajax({
                		url: '<c:url value="/database/clearDataBase"/>',
                		type: "GET",
                        success: function(donnees){
                            alert("Your database is now empty");
                            loadTableSum();
                            $("#barContent").hide();
                        }
                	});	
        		}
        	});
        });
        
        setInterval(function(){progress();}, 10000);
        
        function loadTableSum()
        {
        	$.ajax({
        		url: '<c:url value="/home/loadTable"/>',
        		type: "GET",
                success: function(donnees){
                	$('#tableSum').html(donnees);
                }
        	});
        }
        
        function startShowing()
        {
        	$("#barContent").addClass('progress-striped active');
       		$("#barContent").removeClass('progress-success');
        	progress();
        	$("#barContent").show();
        }
        
        function stopShowing()
        {
        	loadTableSum();
        	
        	$("#barContent").addClass('progress-success');
       		$("#barContent").removeClass('progress-striped active');
       		$("#getProgressBar").css('width','100%');
           	$("#getProgressBar").html('100%');
        }
        
        function progress()
        {
        	if($("#barContent").hasClass('progress-success'))
        		return false;
        	
        	loadTableSum();
        	$.ajax({
        		url: '<c:url value="/database/getProgress"/>',
        		type: "GET",
                success: function(donnees){
                	if(donnees == "Error")
                	{
                		//$('.customBar').hide();
                		//Do nothing for the moment
                	}
                	else
                	{
                		$('.customBar').show();
                		var substr = donnees.split(',');
    	               	var percent = (substr[0]/substr[1])*100;
    	               	percent = percent.toFixed(5);
    	               	if(percent >= 100)
   	               		{
    	               		stopShowing();
    	               		return false;
   	               		}
    	               	
    	               	
    	               	$("#getProgressBar").css('width',percent+'%');
    	               	$("#getProgressBar").html(percent+'%');
                	}
	       			
                }
        	});
        }
        
        

	</script>
	
</body>
</html>
