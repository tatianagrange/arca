<%@ page pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<table class="table table-condensed table-striped">
  <thead>
    <tr>
      <th>Country</th>
      <th class="rightBorder">Sum</th>
      <th>Country</th>
      <th class="rightBorder">Sum</th>
      <th>Country</th>
      <th>Sum</th>
    </tr>
  </thead>
  <tbody>
   	<c:forEach items="${forTable}" varStatus="status" var="line">
   	
   		<c:if test="${status.index%3 == 0}">
   			<tr>
   		</c:if>
   		
       	<th>${line.key.countryName}</th>
       	<th <c:if test="${status.index%3 != 2}">class="rightBorder"</c:if>>${line.value}</th>
       	
       	<c:if test="${status.index%3 == 2}">
   			</tr>
   		</c:if>
   		
   	</c:forEach>
  </tbody>
</table>