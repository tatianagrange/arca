package fr.cestdoncvrai.arca.exercice.Util;

import java.sql.Connection;

public class Conf {
	
	private static String FILE_BIG = "/usr/share/tomcat/webapps/data.txt";
	private static String FILE_SMALL = "/usr/share/tomcat/webapps/smallData.txt";
	public static String FILE = FILE_SMALL;
	
	private static boolean isOnLine = true;
	public static  String DATABASE_SERVER_NAME 	= "localhost";
	public static  String DATABASE_USERNAME = (isOnLine) ? "arca" : "root";
	public static  int DATABASE_PORT = 3306;
	public static  String DATABASE_PASSWORD = (isOnLine) ? "mdpArca" : "";
	public static String DATABASE_NAME = (FILE == FILE_BIG) ? "exercice_all" : "exercice" ;
	

}
