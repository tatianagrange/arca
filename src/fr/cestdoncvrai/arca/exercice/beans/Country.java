package fr.cestdoncvrai.arca.exercice.beans;

import java.util.ArrayList;
import java.util.HashMap;

import fr.cestdoncvrai.arca.exercice.database.QueryCountry;
import fr.cestdoncvrai.arca.exercice.database.QueryData;

public class Country extends Entity {
	
	/************************
	 * 		Attributes		*
	 ************************/
	//Column in base
	private int id;
	private String countryName;
	
	//Object attributes
	ArrayList<Data> datas;
	
	/****************************
	 * 		Constructors		*
	 ****************************/
	public Country() {
		super();
	}
	
	public Country(int id, String countryName) {
		super();
		this.id = id;
		this.countryName = countryName;
	}
	
	public Country(String countryName) {
		super();
		this.countryName = countryName;
	}
	
	/************************
	 * 		Accessors		*
	 ************************/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCountryName() {
		return countryName;
	}
	public void setCountryName(String countryName) {
		this.countryName = countryName;
	}
	public ArrayList<Data> getDatas() {
		return datas;
	}
	public void setDatas(ArrayList<Data> datas) {
		this.datas = datas;
	}
	
	/****************************
	 * 		Public methods		*
	 ****************************/
	@Override
	public void save() {
		QueryCountry qc = new QueryCountry();
		qc.save(this);
		id = qc.getIdFor(this);
	}

	/****************************
	 * 		Statics methods		*
	 ****************************/
	public static void truncate()
	{
		QueryCountry qc = new QueryCountry();
		qc.truncate();
	}

	public static HashMap<String, Integer> getCountryInfo() {
		QueryCountry qc = new QueryCountry();
		ArrayList<Entity> all = qc.getAll();
		HashMap<String,Integer> mapToReturn = new HashMap<String, Integer>();
		for(Entity e : all)
		{
			Country countryTMP = (Country) e;
			mapToReturn.put(countryTMP.getCountryName(),countryTMP.getId());
		}
		return mapToReturn;
	}
}
