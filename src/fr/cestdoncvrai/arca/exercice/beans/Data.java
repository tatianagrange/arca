package fr.cestdoncvrai.arca.exercice.beans;

import java.sql.Timestamp;
import java.util.HashMap;
import java.util.TreeMap;

import fr.cestdoncvrai.arca.exercice.database.QueryData;

public class Data extends Entity{
	
	/************************
	 * 		Attributes		*
	 ************************/
	//Column in base
	private int id;
	private Timestamp timestamp;
	private int number;
	private int countryId;
	
	//Object attributes
	private Country country;
	
	
	/****************************
	 * 		Constructors		*
	 ****************************/
	public Data(int id, Timestamp timestamp, int number, int countryId) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		this.number = number;
		this.countryId = countryId;
	}
	public Data(Timestamp timestamp, int number, int countryId) {
		super();
		this.timestamp = timestamp;
		this.number = number;
		this.countryId = countryId;
	}
	public Data() {
		super();
	}
	
	/************************
	 * 		Accessors		*
	 ************************/
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Timestamp getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}
	public int getNumber() {
		return number;
	}
	public void setNumber(int number) {
		this.number = number;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public Country getCountry() {
		return country;
	}
	public void setCountry(Country country) {
		this.country = country;
	}

	/****************************
	 * 		Public Methods		*
	 ****************************/
	@Override
	public void save() {
		QueryData qd = new QueryData();
		qd.save(this);
	}
	
	/****************************
	 * 		Statics methods		*
	 ****************************/
	//Thoses functions are static because they are about every Datas, not juste one.
	public static void truncate()
	{
		QueryData qd = new QueryData();
		qd.truncate();
	}
	
	public static HashMap<Country, Long> getAllSumByCountry()
	{
		QueryData qd = new QueryData();
		return qd.getSumGroupBy("country_id");
	}
	
	public static TreeMap<Timestamp, Long> getDataOnOneYear(int year)
	{
		QueryData qd = new QueryData();
		return qd.getSumsForYear(year);
	}
}
