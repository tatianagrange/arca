package fr.cestdoncvrai.arca.exercice.beans;

import fr.cestdoncvrai.arca.exercice.database.QueryCountry;
import fr.cestdoncvrai.arca.exercice.database.QueryFileInformation;
import fr.cestdoncvrai.arca.exercice.exceptions.DoesntExistException;

public class FileInformation extends Entity {

	private int id;
	private String name;
	private int lineNumber;
	private int lastReadLine;
	private int isActive;
	
	public FileInformation(String name) throws DoesntExistException
	{
		super(); 
		this.name = name;
		if(!this.exist())
			throw new DoesntExistException();
	}
	
	public FileInformation(String name, int lineNumber) {
		super();
		this.name = name;
		this.lineNumber = lineNumber;
		this.lastReadLine = 0;
	}
	

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @return the isActive
	 */
	public int getIsActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the lineNumber
	 */
	public int getLineNumber() {
		return lineNumber;
	}

	/**
	 * @param lineNumber the lineNumber to set
	 */
	public void setLineNumber(int lineNumber) {
		this.lineNumber = lineNumber;
	}

	/**
	 * @return the lastReadLine
	 */
	public int getLastReadLine() {
		return lastReadLine;
	}

	/**
	 * @param lastReadLine the lastReadLine to set
	 */
	public void setLastReadLine(int lastReadLine) {
		this.lastReadLine = lastReadLine;
	}
	
	public void changeActivity(boolean start)
	{
		isActive = (start) ? 1 : 0;
		QueryFileInformation qfi = new QueryFileInformation();
		qfi.updateActivity(this);
	}
	
	public void saveLastReadLine()
	{
		QueryFileInformation qfi = new QueryFileInformation();
		qfi.updateLastReadLine(this);
	}

	@Override
	public void save() {
		QueryFileInformation qfi = new QueryFileInformation();
		qfi.save(this);
		try {
			id = qfi.getIdFor(this);
		} catch (DoesntExistException e) {
			e.printStackTrace();
		}
	}


	public boolean exist() {
		QueryFileInformation qfi = new QueryFileInformation();
		try
		{
			id = qfi.getIdFor(this);
			lastReadLine = ((FileInformation)qfi.findById(id)).getLastReadLine();
			lineNumber = ((FileInformation)qfi.findById(id)).getLineNumber();
		}
		catch(DoesntExistException e)
		{
			return false;
		}
		return true;
	}
	
	public void checkActivity()
	{
		QueryFileInformation qfi = new QueryFileInformation();
		try
		{
			id = qfi.getIdFor(this);
			isActive = ((FileInformation)qfi.findById(id)).getIsActive();
		}
		catch(DoesntExistException e)
		{
			e.printStackTrace();
		}
	}
	
	/****************************
	 * 		Statics methods		*
	 ****************************/
	public static void truncate()
	{
		QueryFileInformation qfi = new QueryFileInformation();
		qfi.truncate();
	}

}
