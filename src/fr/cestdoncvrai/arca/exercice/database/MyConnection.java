package fr.cestdoncvrai.arca.exercice.database;

import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import javax.swing.text.html.HTMLDocument.HTMLReader.IsindexAction;

import fr.cestdoncvrai.arca.exercice.Util.Conf;

public class MyConnection {
	
	private String server 	= Conf.DATABASE_SERVER_NAME;
	private String username = Conf.DATABASE_USERNAME;
	private int port = Conf.DATABASE_PORT;
	private String password = Conf.DATABASE_PASSWORD;
	private String dbName 	= Conf.DATABASE_NAME;
	private Connection currentConnection;

	public MyConnection() {
		//Load driver
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the server
	 */
	public String getServer() {
		return server;
	}
	/**
	 * @param server the server to set
	 */
	public void setServer(String server) {
		this.server = server;
	}
	/**
	 * @return the port
	 */
	public int getPort() {
		return port;
	}
	/**
	 * @param port the port to set
	 */
	public void setPort(int port) {
		this.port = port;
	}
	/**
	 * @return the username
	 */
	public String getUsername() {
		return username;
	}
	/**
	 * @param username the username to set
	 */
	public void setUsername(String username) {
		this.username = username;
	}
	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the dbName
	 */
	public String getDbName() {
		return dbName;
	}
	/**
	 * @param dbName the dbName to set
	 */
	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	/**
	 * @return the currentConnection
	 */
	public Connection getCurrentConnection() {
		return currentConnection;
	}
	/**
	 * @param currentConnection the currentConnection to set
	 */
	public void setCurrentConnection(Connection currentConnection) {
		this.currentConnection = currentConnection;
	}

	public void execut(String request)
	{
		Connection co = null;
		@SuppressWarnings("unused")
		int statut;
		try {
			co = DriverManager.getConnection("jdbc:mysql://"+server+":"+port+"/"+dbName,username, password);
			Statement statement = co.createStatement();
			statut = statement.executeUpdate(request);
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if(co != null) {
				try {
					co.close();
				} catch (SQLException e) {
					//The error has to be ignored
				}
			}
		}
	}

	public ArrayList<HashMap> resultFor(String request){
		Connection co = null;
		ArrayList<HashMap> toReturn = null;
		try {
			co = DriverManager.getConnection("jdbc:mysql://"+server+":"+port+"/"+dbName,username, password);
			Statement statement = co.createStatement();
			toReturn = resultSetToArrayList(statement.executeQuery(request));
		} catch (Exception e) {
			//Connection fail
		} finally {
			if(co != null) {
				try {
					co.close();
				} catch (SQLException e) {
					//The error has to be ignored
				}
			}
		}

		return toReturn;
	}

	private ArrayList<HashMap> resultSetToArrayList(ResultSet rs) throws SQLException{
		ResultSetMetaData md = rs.getMetaData();
		int columns = md.getColumnCount();
		ArrayList<HashMap> list = new ArrayList<HashMap>(50);
		while (rs.next()){
			//each row is an hashmap 
			HashMap row = new HashMap(columns);
			for(int i=1; i<=columns; ++i){
				Object o = rs.getObject(i);
				
				if (o instanceof Timestamp) {
					java.util.Calendar cal = Calendar.getInstance(); 
					row.put(md.getColumnName(i),rs.getString(i));
				}else
					row.put(md.getColumnName(i),o);
			}
			list.add(row);
		}
		return list;
	}
}
