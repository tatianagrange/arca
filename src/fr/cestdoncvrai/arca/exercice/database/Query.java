package fr.cestdoncvrai.arca.exercice.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fr.cestdoncvrai.arca.exercice.beans.Entity;
import fr.cestdoncvrai.arca.exercice.exceptions.DoesntExistException;

public abstract class Query {
	protected String table;
	protected MyConnection connexion;
	
	public Query() {
		connexion = new MyConnection();
	}
	
	public Entity findById(int id){
		ArrayList result = connexion.resultFor("select * from " + table + " where id = " + id);
		return parseOne(result);
	}
	
	public void truncate()
	{
		connexion.execut("Alter table exercice.data drop foreign key fk_data_country");
		connexion.execut("TRUNCATE " + table);
		connexion.execut("ALTER TABLE `exercice`.`data`    ADD CONSTRAINT `fk_data_country`   FOREIGN KEY (`country_id` )   REFERENCES `exercice`.`country` (`id` )");
	}
	
	public ArrayList<Entity> getAll()
	{
		ArrayList result = connexion.resultFor("select * from " + table);
		return parseMany(result);
	}
	
	public abstract int getIdFor(Entity entity) throws DoesntExistException;

	public abstract Entity parseOne(ArrayList result);
	public abstract ArrayList<Entity> parseMany(ArrayList result);
	
	public abstract void save(Entity entity);	
}
