package fr.cestdoncvrai.arca.exercice.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fr.cestdoncvrai.arca.exercice.beans.Country;
import fr.cestdoncvrai.arca.exercice.beans.Data;
import fr.cestdoncvrai.arca.exercice.beans.Entity;

public class QueryCountry extends Query {

	public QueryCountry() {
		super();
		table = "exercice.country";
	}
	
	@Override
	public Entity parseOne(ArrayList result) 
	{
		Iterator<HashMap> it = result.iterator();
		HashMap map = it.next();
		int id = (Integer) map.get("id");
		String countryName = (String) map.get("country_name") ;
		return new Country(id, countryName);
	}
	
	@Override
	public ArrayList<Entity> parseMany(ArrayList result) {
		Iterator<HashMap> it = result.iterator();
		ArrayList<Entity> toReturn = new ArrayList<Entity>();
		
		while(it.hasNext())
		{
			HashMap map = it.next();
			int id = (Integer) map.get("id");
			String countryName = (String) map.get("country_name") ;
			toReturn.add(new Country(id, countryName));
		}
		
		return toReturn;
	}
	
	@Override
	public void save(Entity country) {
		connexion.execut("INSERT INTO " + table + " (country_name) VALUES ('" + ((Country)country).getCountryName() + "')");
	}
	
	@Override
	public int getIdFor(Entity country) {
		ArrayList result = connexion.resultFor("select id from " + table + " where country_name = '" + ((Country)country).getCountryName() + "'");
		Iterator<HashMap> it = result.iterator();
		HashMap map = it.next();
		return (Integer) map.get("id");
	}
}