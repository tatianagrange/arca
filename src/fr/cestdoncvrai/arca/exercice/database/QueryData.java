package fr.cestdoncvrai.arca.exercice.database;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeMap;

import fr.cestdoncvrai.arca.exercice.beans.Country;
import fr.cestdoncvrai.arca.exercice.beans.Data;
import fr.cestdoncvrai.arca.exercice.beans.Entity;

public class QueryData extends Query {
	public QueryData() {
		super();
		table = "exercice.data";
	}

	@Override
	public Entity parseOne(ArrayList result) 
	{
		Iterator<HashMap> it = result.iterator();
		HashMap map = it.next();
		int id = (Integer) map.get("id");
		Timestamp timestamp = (Timestamp) map.get("timestamp");
		int number = (Integer) map.get("number");
		int countryId = (Integer) map.get("country_id");
		return new Data(id, timestamp, number, countryId);
	}

	@Override
	public ArrayList<Entity> parseMany(ArrayList result) {
		Iterator<HashMap> it = result.iterator();
		ArrayList<Entity> toReturn = new ArrayList<Entity>();
		
		while(it.hasNext())
		{
			HashMap map = it.next();
			int id = (Integer) map.get("id");
			Timestamp timestamp = (Timestamp) map.get("timestamp");
			int number = (Integer) map.get("number");
			int countryId = (Integer) map.get("country_id");
			toReturn.add(new Data(id, timestamp, number, countryId));
		}
		
		return toReturn;
	}
	
	@Override
	public void save(Entity data) {
		connexion.execut("INSERT INTO " + table + " (timestamp,number,country_id) VALUES ('" + ((Data)data).getTimestamp() + "', " +((Data)data).getNumber()+ ", " + ((Data)data).getCountryId() + ")");
	}

	@Override
	public int getIdFor(Entity data) {
		ArrayList result = connexion.resultFor("select id from " + table + " where number = " + ((Data)data).getNumber() + " and timestamp = " + ((Data)data).getTimestamp());
		Iterator<HashMap> it = result.iterator();
		HashMap map = it.next();
		return (Integer) map.get("id");
	}

	public HashMap<Country,Long> getSumGroupBy(String attribut) {
		HashMap<Country,Long> toReturn = new HashMap<Country,Long>();
		ArrayList result = connexion.resultFor("select " + attribut + ", sum(number) from exercice.data group by " + attribut);
		Iterator<HashMap> it = result.iterator();
		while(it.hasNext())
		{
			HashMap map = it.next();
			int id = (Integer)map.get("country_id");
			QueryCountry qd = new QueryCountry();
			Country c = (Country) qd.findById(id);
			long sum = ((BigDecimal) map.get("sum(number)")).longValue();
			
			toReturn.put(c, sum);
		}
		
		return toReturn;
	}
	
	public TreeMap<Timestamp, Long> getSumsForYear(int year)
	{
		TreeMap<Timestamp,Long> toReturn = new TreeMap<Timestamp,Long>();
		ArrayList result = connexion.resultFor("SELECT timestamp, sum(number) FROM data group by date(timestamp)");
		
		Iterator<HashMap> it = result.iterator();
		while(it.hasNext())
		{
			HashMap map = it.next();
			//Timestamp ts = (Timestamp)map.get("timestamp");
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.S");
			Date parsedDate;
			try {
				parsedDate = dateFormat.parse((String)map.get("timestamp"));
				Timestamp ts = new java.sql.Timestamp(parsedDate.getTime());
				long sum = ((BigDecimal) map.get("sum(number)")).longValue();
				toReturn.put(ts, sum);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			
			
		}
		
		return toReturn;
		
	}
}
