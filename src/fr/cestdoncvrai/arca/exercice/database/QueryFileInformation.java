package fr.cestdoncvrai.arca.exercice.database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import fr.cestdoncvrai.arca.exercice.beans.Country;
import fr.cestdoncvrai.arca.exercice.beans.Data;
import fr.cestdoncvrai.arca.exercice.beans.Entity;
import fr.cestdoncvrai.arca.exercice.beans.FileInformation;
import fr.cestdoncvrai.arca.exercice.exceptions.DoesntExistException;

public class QueryFileInformation extends Query {
	
	public QueryFileInformation() {
		super();
		this.table = "exercice.file_information";
	}

	@Override
	public int getIdFor(Entity fileInfo) throws DoesntExistException {
		ArrayList result = connexion.resultFor("select id from " + table + " where name = '" + ((FileInformation)fileInfo).getName() + "'");
		if (result.size() == 0)
			throw new DoesntExistException();
		Iterator<HashMap> it = result.iterator();
		HashMap map = it.next();
		return (Integer) map.get("id");
	}

	@Override
	public Entity parseOne(ArrayList result) {
		Iterator<HashMap> it = result.iterator();
		HashMap map = it.next();
		int id = (Integer) map.get("id");
		String name  = (String) map.get("name") ;
		int lastReadLine = (Integer) map.get("last_read_line");
		int numberOfLine = (Integer) map.get("line_number");
		int isActive = (Integer)map.get("is_active");
		FileInformation fi = new FileInformation(name, numberOfLine);
		fi.setId(id);
		fi.setLastReadLine(lastReadLine);
		fi.setIsActive(isActive);
		return fi;
	}

	@Override
	public ArrayList<Entity> parseMany(ArrayList result) {
		// TODO Auto-generated method stub
		return null;
	}
	
	public void updateActivity(FileInformation fileInfo)
	{
		connexion.execut("UPDATE " + table + " set  is_active = " + fileInfo.getIsActive() + " where id = " +fileInfo.getId() );
	}

	
	public void updateLastReadLine(FileInformation fileInfo)
	{
		connexion.execut("UPDATE " + table + " set  last_read_line = " + fileInfo.getLastReadLine() + " where id = " +fileInfo.getId() );
	}

	@Override
	public void save(Entity fileInfo) {
		connexion.execut("INSERT INTO " + table + " (name,line_number,last_read_line,is_active) VALUES ('" + ((FileInformation)fileInfo).getName() + "', " +((FileInformation)fileInfo).getLineNumber()+ ", " + ((FileInformation)fileInfo).getLastReadLine() + ",0)");
	}

}
