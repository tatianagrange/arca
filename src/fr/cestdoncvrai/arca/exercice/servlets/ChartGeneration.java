package fr.cestdoncvrai.arca.exercice.servlets;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cestdoncvrai.arca.exercice.beans.Data;

public class ChartGeneration extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		TreeMap<Timestamp,Long> map = Data.getDataOnOneYear(0);
		Set cles = map.keySet();
		Iterator it = cles.iterator();
		String json = "";
		while (it.hasNext()){
		   Timestamp cle = (Timestamp)it.next();
		   long l = (Long)map.get(cle);
		   
		   json += (cle.getTime()/1000) + "," + l + "//";
		}
		json = json.substring(0, json.length()-2);
		resp.getWriter().println(json);
	}

}
