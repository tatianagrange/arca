package fr.cestdoncvrai.arca.exercice.servlets;

import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import au.com.bytecode.opencsv.CSVReader;

import java.util.*;
import java.io.*;

import fr.cestdoncvrai.arca.exercice.Util.Conf;
import fr.cestdoncvrai.arca.exercice.beans.Country;
import fr.cestdoncvrai.arca.exercice.beans.Data;
import fr.cestdoncvrai.arca.exercice.beans.FileInformation;
import fr.cestdoncvrai.arca.exercice.exceptions.DoesntExistException;

public class DataBaseGeneration extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		
		String fileName = Conf.FILE;
		
		FileInformation fileInfo;
		try {
			fileInfo = new FileInformation(fileName);
		} catch (DoesntExistException e) {
			//Count number of line in the file
			Scanner file = new Scanner(new File(fileName));
		    int count = 0;
		    while (file.hasNextLine()) 
		    {
		    	  count++;
		    	  file.nextLine();
		    }
		    fileInfo = new FileInformation(fileName, count);
	    	fileInfo.save();
		}
		if(fileInfo.getIsActive() == 1)	return;
		fileInfo.changeActivity(true);
		
	    
	    //Check if the file has already been all treated
	    if (fileInfo.getLastReadLine() == fileInfo.getLineNumber())
	    {
	    	resp.getWriter().println("This file has already been treated.");
	    	return;
	    }
	    
	    //CSVReader is used to read the file line by line.
		CSVReader reader = new CSVReader(new InputStreamReader(new FileInputStream(fileName), "UTF-8"));
		//This variable will contain each line of the file
		String[] row = null;		
		//Get the country already in base to know there id and limited the database access
		HashMap<String,Integer> rememberCountries = Country.getCountryInfo();
		//Count the number of lines
		int i = 0;
		
		while((row = reader.readNext()) != null)
		{
			i++;
			if(i > fileInfo.getLastReadLine()) //This is to start the treatment where we stopped last time.
			{
				if(!rememberCountries.containsKey(row[2]))
				{
					//Save the country only if it is not in base
					Country countryToSave = new Country(row[2]);
					countryToSave.save();
					rememberCountries.put(row[2], countryToSave.getId());
				}
				
				//Save the datas
				Data dataToSave = new Data(new Timestamp(Long.parseLong(row[0])), Integer.parseInt(row[1]), rememberCountries.get(row[2]));
				dataToSave.save();
				
				
				//Update the state of the file.
				fileInfo.setLastReadLine(fileInfo.getLastReadLine()+1);
				if(i%2500 == 0)
				{
					fileInfo.saveLastReadLine();
					fileInfo.checkActivity();
					if(fileInfo.getIsActive() == 0)
						return;
				}
			}
		}
	}

}
