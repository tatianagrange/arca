package fr.cestdoncvrai.arca.exercice.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cestdoncvrai.arca.exercice.beans.Country;
import fr.cestdoncvrai.arca.exercice.beans.Data;

public class Home extends HttpServlet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//Table generation
/*		HashMap<Country,Long> map = Data.getAllSumByCountry();
		
		req.setAttribute("forTable", map);*/
		
		this.getServletContext().getRequestDispatcher( "/WEB-INF/home.jsp" ).forward( req, resp );
	}

}
