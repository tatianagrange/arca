package fr.cestdoncvrai.arca.exercice.servlets;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cestdoncvrai.arca.exercice.beans.Country;
import fr.cestdoncvrai.arca.exercice.beans.Data;

public class LoadTable extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		HashMap<Country,Long> map = Data.getAllSumByCountry();
		if(map.size() == 0)
		{
			resp.getWriter().println("<p>There are no data in base.</p>");
			return;
		}
		req.setAttribute("forTable", map);
		this.getServletContext().getRequestDispatcher( "/inc/table.jsp" ).forward( req, resp );
	}
}
