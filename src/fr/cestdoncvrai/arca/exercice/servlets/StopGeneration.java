package fr.cestdoncvrai.arca.exercice.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.cestdoncvrai.arca.exercice.Util.Conf;
import fr.cestdoncvrai.arca.exercice.beans.FileInformation;
import fr.cestdoncvrai.arca.exercice.exceptions.DoesntExistException;

public class StopGeneration extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		//Get the file informations
		String fileName = Conf.FILE;
		FileInformation fileInfo;
		try {
			fileInfo = new FileInformation(fileName);
			fileInfo.changeActivity(false);
		} catch (DoesntExistException e) {
			return;
		}
	}
}
